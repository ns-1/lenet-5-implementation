import jax.numpy as jnp
import numpy as np
from jax import grad, jit, vmap
from jax import random
from jax import nn
import optax
from tqdm import tqdm
import pickle
import math 
import time

from jax import lax

jit_sigmoid = jit(nn.sigmoid)

@jit
def avg_pool(input):
    """Average Pooling Function -- Uses window of 2 x 2"""
    reduce_window_args = (0., lax.add, (1, 1, 2, 2), (1, 1, 2, 2), "VALID")
    out = lax.reduce_window(input, *reduce_window_args)
    s = out / 4
    return s
    
@jit
def accuracy(params, x_data, y_true):
    """Accuracy Score Function"""
    y_pred_probs = forward(x_data, params)  
    y_pred_probs = nn.softmax(y_pred_probs)
    y_pred_labels = jnp.argmax(y_pred_probs, axis=1)

    correct_predictions = jnp.sum(y_pred_labels == jnp.argmax(y_true, axis=1))
    accuracy = correct_predictions / len(y_true)
    return accuracy
    
@jit
def loss(params: optax.Params, batch: jnp.ndarray, labels: jnp.ndarray) -> jnp.ndarray:
    """Calculates loss for network, utilizes softmax cross entropy"""
    y_hat = forward(batch, params)
    loss_value = jnp.mean(optax.softmax_cross_entropy(y_hat, labels))
    return loss_value

@jit
def forward(x: jnp.ndarray, params: optax.Params) -> jnp.ndarray:
    """Forward propagation function"""
    x = lax.conv(x, params['conv1'], (1, 1), 'SAME')
    x = jit_sigmoid(x)
    x = avg_pool(x)
    x = lax.conv(x, params['conv2'], (1, 1), 'VALID')
    x = jit_sigmoid(x)
    x = avg_pool(x)
    a, b, c, d = x.shape
    x = lax.reshape(x, (a, b * c * d))
    x = jnp.dot(x, params['input']) + params['bias_in']
    x = jit_sigmoid(x)
    x = jnp.dot(x, params['hidden']) + params['bias_l1']
    x = jit_sigmoid(x)
    x = jnp.dot(x, params['output']) + params['bias_l2']
    return x

class CNN:
    def __init__(self, seed: int = 42):
        """Initializes params for network, based on LeNet-5 Architecture"""

        channels_in1 = 1
        channels_out1 = 6
        kernel_size1 = 5
        channels_in2 = 6
        channels_out2 = 16
        kernel_size2 = 5
        input_size = 120
        hidden_size = 84
        output_size = 10
        
        key, key1, key2, key3, key4, key5, key6, key7, key8 = random.split(random.PRNGKey(seed), num=9)

        self.params = {
            'conv1': random.normal(shape=[channels_out1, channels_in1, kernel_size1, kernel_size1], key=key5),
            'conv2': random.normal(shape=[channels_out2, channels_in2, kernel_size2, kernel_size2], key=key6),
            'input': random.normal(shape=[channels_out2 * kernel_size2 * kernel_size2, input_size], key=key7),
            'hidden': random.normal(shape=[input_size, hidden_size], key=key1),
            'output': random.normal(shape=[hidden_size, output_size], key=key2),
            'bias_in': random.normal(shape=[input_size], key=key8),
            'bias_l1': random.normal(shape=[hidden_size,], key=key3), 
            'bias_l2': random.normal(shape=[output_size,], key=key4)
        }

        self.key = key

    def train(self, x_train, y_train, x_val, y_val, batch_size = 128, learning_rate: float = 1e-3, epochs=60):
        """Training loop"""
        optimizer = optax.adam(learning_rate)
        params = self.params    
        opt_state = optimizer.init(params)
        best_loss = 100

        for i in range(epochs):
            start_time = time.time()
            print(f"=== Epoch {i} ===")
            batch_count = 0
            for j in range(0, len(x_train), batch_size): 
                if batch_count % 100 == 0:
                    avg_loss = []
                    avg_accuracy = []
                    nums = 0
                    for k in range(0, len(x_val), batch_size):  
                        curr_images = x_val[k:k + batch_size]
                        curr_labels = y_val[k:k + batch_size]
                        one_hot_labels = nn.one_hot(curr_labels, 10)
                        a, b, c = curr_images.shape
                        curr_images = jnp.reshape(curr_images, (a, 1, b, c))     
                        one_hot_labels = nn.one_hot(curr_labels, 10)
                        l = loss(params, curr_images, one_hot_labels)
                        avg_loss.append(l)
                        avg_accuracy.append(accuracy(params, curr_images, one_hot_labels))
                        nums = k 

                    loss_ = sum(avg_loss)/len(avg_loss)

                    if(loss_ < best_loss):
                        best_loss = loss_
                        self.save_params()

                    print(f"Batch Iter: {batch_count} Validation Accuracy: {np.mean(avg_accuracy)}")
                    
                     
                curr_images = x_train[j:j + batch_size]
                curr_labels = y_train[j:j + batch_size]
                one_hot_labels = nn.one_hot(curr_labels, 10)
                a, b, c = curr_images.shape
                curr_images = jnp.reshape(curr_images, (a, 1, b, c))      
                grads = grad(loss)(params, curr_images, one_hot_labels)
                updates, opt_state = optimizer.update(grads, opt_state)
                params = optax.apply_updates(params, updates)
                self.params = params
                batch_count+=1

                epoch_time = time.time() - start_time
            
            print("Epoch {} in {:0.2f} sec".format(i, epoch_time))

        self.save_params()

    def save_params(self):
        with open('cnn_params.pkl', 'wb') as f:
            pickle.dump(self.params, f)

    def load_params(self):
        with open('cnn_params.pkl', 'rb') as f:
            saved_params = pickle.load(f)
            self.params = saved_params