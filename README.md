# LeNet-5 Implementation

My implementation of LeNet-5 for MNIST classification, uses the original architecture as in the 1998 paper [Gradient Based Learning Applied to Document Recognition](http://vision.stanford.edu/cs598_spring07/papers/Lecun98.pdf). Accuracy of 98.5% on test set for best model. Utilized batch size of 128, learning rate of 1e-3, trained with early stopping. 

![image](convolution.png)