from cnn import CNN
import cnn as cn
import jax.numpy as jnp
from jax import random
import data_utils as dl
import math
from jax import nn 
import time

train_images, train_labels, test_images, test_labels = dl.load_data()

train_images = dl.scale(train_images)
test_images = dl.scale(test_images)

randomized_indices = random.permutation(random.PRNGKey(42), jnp.arange(train_images.shape[0]))
split = math.ceil(train_images.shape[0] * .9)
train_idx, valid_idx = randomized_indices[:split], randomized_indices[split:]

x_train, y_train = train_images[train_idx,:], train_labels[train_idx]
x_val, y_val = train_images[valid_idx, :], train_labels[valid_idx]

network = CNN()

start_time = time.time()
network.train(x_train, y_train, x_val, y_val)
train_time = time.time() - start_time
print("Trained in {:0.2f} sec".format(train_time))
network.load_params()

avg_accuracy = []
batch_size = 1024
for k in range(0, len(test_images), batch_size):  
    curr_images = test_images[k:k + batch_size]
    curr_labels = test_labels[k:k + batch_size]
    one_hot_labels = nn.one_hot(curr_labels, 10)
    a, b, c = curr_images.shape
    curr_images = jnp.reshape(curr_images, (a, 1, b, c))     
    one_hot_labels = nn.one_hot(curr_labels, 10)
    avg_accuracy.append(cn.accuracy(network.params, curr_images, one_hot_labels))

acc = sum(avg_accuracy)/len(avg_accuracy)

print(f"Test Set Accuracy w/ Best Model: {acc}")