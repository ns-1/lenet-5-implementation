import matplotlib.pyplot as plt
import data_utils as dl
from cnn import CNN
from jax import lax

_, _, test_images, _ = dl.load_data()
im = test_images[42]
im = dl.scale(im)
network = CNN()

network.load_params()

out = lax.conv(im.reshape(1, 1, 28, 28), network.params['conv1'], (1, 1), 'SAME')

fig, axs = plt.subplots(2, 4)
fig.set_figheight(10)
fig.set_figwidth(10)
fig.suptitle('Feature Maps generated from Conv Layer 1', fontsize=16)

axs[0, 0].imshow(im)
axs[0, 0].title.set_text("Original")

axs[0, 1].imshow(out[0][0])
axs[0, 1].title.set_text("Feature Map 1")

axs[0, 2].imshow(out[0][1])
axs[0, 2].title.set_text("Feature Map 2")

axs[0, 3].imshow(out[0][2])
axs[0, 3].title.set_text("Feature Map 3")

axs[1, 0].imshow(out[0][3])
axs[1, 0].title.set_text("Feature Map 4")

axs[1, 1].imshow(out[0][4])
axs[1, 1].title.set_text("Feature Map 5")

axs[1, 2].imshow(out[0][5])
axs[1, 2].title.set_text("Feature Map 6")

axs[1, 3].axis("off")

plt.savefig('convolution.png')