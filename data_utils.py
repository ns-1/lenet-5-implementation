import struct
import jax.numpy as jnp
import numpy as np
import os

def scale(tensor):
    return tensor / 255

def load_mnist_images(file_path):
    with open(file_path, 'rb') as f:
        magic, num, rows, cols = struct.unpack('>IIII', f.read(16))
        images = jnp.asarray(np.fromfile(f, dtype=np.uint8).reshape(num, rows, cols))
    return images

def load_mnist_labels(file_path):
    with open(file_path, 'rb') as f:
        magic, num = struct.unpack('>II', f.read(8))
        labels = jnp.asarray(np.fromfile(f, dtype=np.uint8))
    return labels

def load_data():
    script_dir = os.path.dirname(os.path.abspath(__file__))

    train_images_path = 'data/train-images.idx3-ubyte'
    train_labels_path = 'data/train-labels.idx1-ubyte'
    test_images_path = 'data/t10k-images.idx3-ubyte'
    test_labels_path = 'data/t10k-labels.idx1-ubyte'

    train_images_path = os.path.join(script_dir, train_images_path)
    train_labels_path = os.path.join(script_dir, train_labels_path)
    test_images_path = os.path.join(script_dir, test_images_path)
    test_labels_path = os.path.join(script_dir, test_labels_path)

    train_images = load_mnist_images(train_images_path)
    train_labels = load_mnist_labels(train_labels_path)
    test_images = load_mnist_images(test_images_path)
    test_labels = load_mnist_labels(test_labels_path)

    return train_images, train_labels, test_images, test_labels